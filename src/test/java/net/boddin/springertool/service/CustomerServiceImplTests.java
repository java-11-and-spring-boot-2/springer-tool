package net.boddin.springertool.service;

import net.boddin.springertool.domain.Abo;
import net.boddin.springertool.repository.AboRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

public class CustomerServiceImplTests {

    @Test
    public void test(){
        var aboRepo = Mockito.mock(AboRepository.class);

        var list = new ArrayList<Abo>();
        list.add(new Abo("druck", 1));

        Mockito.when(aboRepo.findAll()).thenReturn(list);

        Mockito.when(aboRepo.save(Mockito.any(Abo.class))).thenReturn(null);

        var customerService = new CustomerServiceImpl(aboRepo);

        var ret = customerService.getAbos();

        assert ret.size() == list.size();
        Assertions.assertThat(ret).allMatch(el -> el.getDruckErzeugnis() != null);
        Assertions.assertThat(ret.size()).isEqualTo(list.size());

    }
}
