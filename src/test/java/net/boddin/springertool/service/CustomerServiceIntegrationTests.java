package net.boddin.springertool.service;

import net.boddin.springertool.repository.AboRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerServiceIntegrationTests {
    @Autowired
    private CustomerService customerService;

    @Autowired
    private TestEntityManager entityManager;

    @TestConfiguration
    static class TestContextConfiguration {

        @MockBean
        private AboRepository aboRepository;

        @Bean
        public CustomerService contactService() {
            return new CustomerServiceImpl(aboRepository);
        }

    }

    @Test
    public void test(){

    }

}
