package net.boddin.springertool.repository;

import net.boddin.springertool.domain.Abo;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Optional;


public interface AboRepository extends CrudRepository<Abo, Long> {
    Optional<Abo> findFirstByAuftragsNummer(long nr);
    Collection<Abo> findAbosByAuftragsNummer(long nr);
}
