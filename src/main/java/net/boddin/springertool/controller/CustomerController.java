package net.boddin.springertool.controller;

import net.boddin.springertool.config.MyConfig;
import net.boddin.springertool.domain.Abo;
import net.boddin.springertool.dto.AboDto;
import net.boddin.springertool.exception.NotFoundException;
import net.boddin.springertool.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("customer")
public class CustomerController {


    private final CustomerService customerService;
    private final MyConfig myConfig;

    @Autowired
    public CustomerController(@Qualifier("customerService1") CustomerService customerService, MyConfig myConfig) {
        this.customerService = customerService;
        this.myConfig = myConfig;
    }

    @GetMapping("/config")
    public MyConfig getConfig(){
        return myConfig;
    }

    @GetMapping("/abos")
    @Secured({"ROLE_ANON", "ROLE_USER"})
    public Collection<Abo> getMyAbos(){
        return this.customerService.getAbos();
    }

    @PostMapping("/abos/add")
    @Secured("ROLE_ADMIN")
    public void addAbo(@Valid @RequestBody AboDto dto,
                       BindingResult result,
                       HttpServletResponse resp){

        result.getAllErrors().forEach(err ->{
            System.err.println("error: " + err.getDefaultMessage());
        });
        if(result.hasErrors()){
            resp.setStatus(400);
            return;
        }
        this.customerService.saveAbo(
                new Abo(dto.getBezeichnung(),
                        dto.getAuftragsnummer()));
    }


    @GetMapping("/abos/{nr}")
    public Abo getAboByNr(@PathVariable("nr") long nr){

//        var optionalAbo = customerService.getByAuftragsNummer(nr);
//        if(optionalAbo.isPresent()){
//            return optionalAbo.get();
//        } else {
//            throw new NotFoundException();
//        }
        return this.customerService
                .getByAuftragsNummer(nr)
                .orElseThrow(NotFoundException::new);
    }

}
