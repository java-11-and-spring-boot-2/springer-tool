package net.boddin.springertool.controller;

import net.boddin.springertool.domain.Abo;
import net.boddin.springertool.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class AboController {

    private final CustomerService customerService;

    @Autowired
    public AboController(@Qualifier("customerService1") CustomerService customerService) {
        this.customerService = customerService;
    }

    @RequestMapping({"/", "/list"})
    public String showIndex(Model model){
        var abos = customerService.getAbos();
        model.addAttribute("abos", abos);
        return "index";
    }

    @RequestMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model){
        var abo = this.customerService.getAbosAsStream().filter(el -> el.getId() == id).findFirst();
        if(abo.isPresent()){
            model.addAttribute("abo", abo.get());
            return "add-abo";
        }
        return "redirect:/list";
    }


    @RequestMapping("/add-abo")
    public String showAddAboForm(Abo abo, Model model){
        model.addAttribute("abo", abo);
        return "add-abo";
    }

    @PostMapping("/add")
    public String addAbo(@Valid Abo abo, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-abo";
        }

        this.customerService.saveAbo(
                abo);
        return "redirect:list";
    }
}
