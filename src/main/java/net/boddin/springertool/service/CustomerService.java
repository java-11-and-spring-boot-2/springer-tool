package net.boddin.springertool.service;

import net.boddin.springertool.domain.Abo;
import net.boddin.springertool.repository.AboRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

public interface CustomerService {

    Collection<Abo> getAbos();

    Optional<Abo> getByAuftragsNummer(long nr);

    Stream<Abo> getAbosAsStream();

    void saveAbo(Abo abo);
}
