package net.boddin.springertool.service;

import net.boddin.springertool.domain.Abo;
import net.boddin.springertool.repository.AboRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("customerService1")
@Primary
public class CustomerServiceImpl implements CustomerService {

    private final AboRepository aboRepository;

    @Value("${my.property}")
    private String myProperty;

    public CustomerServiceImpl(AboRepository aboRepository) {
        this.aboRepository = aboRepository;

        var abo1 = new Abo("Welt", 123);
        abo1.setTime(LocalDateTime.now());
        this.aboRepository.save(abo1);

        var abo2 = new Abo("Bild", 456);
        this.aboRepository.save(abo2);

        var abo3 = new Abo("Bild2", 4567);
        this.aboRepository.save(abo3);
    }

    @Override
    public Collection<Abo> getAbos() {
        return getAbosAsStream().collect(Collectors.toList());
    }

    @Override
    public Optional<Abo> getByAuftragsNummer(long nr) {
        return this.aboRepository.findFirstByAuftragsNummer(nr);
    }

    private Optional<Abo> getByAuftragsNummerOld(long nr) {
        Stream<Abo> filtered = getAbosAsStream()
                .filter(abo -> abo.getAuftragsNummer() == nr);
        return filtered.findFirst();
    }

    @Override
    public Stream<Abo> getAbosAsStream() {
        Iterable<Abo> iterableAbos = this.aboRepository.findAll();
        var list = new ArrayList<Abo>();
        iterableAbos.forEach(element -> list.add(element));
        return list.stream();
    }

    @Override
    public void saveAbo(Abo abo) {
        this.aboRepository.save(abo);
    }
}
