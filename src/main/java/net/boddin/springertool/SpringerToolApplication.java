package net.boddin.springertool;

import net.boddin.springertool.repository.AboRepository;
import net.boddin.springertool.service.CustomerService;
import net.boddin.springertool.service.CustomerServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@SpringBootApplication
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SpringerToolApplication {

	//@Bean("customerService2")
	public CustomerService customerService1(AboRepository repository){
		return new CustomerServiceImpl(repository);
	}

	@Bean
	public UserDetailsService userDetailsService() throws Exception {
		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
		manager.createUser(User
				.withUsername("user")
				.password(defaultpasswordEncoder().encode("user"))
				.roles("USER").build());
		manager.createUser(User
				.withUsername("admin")
				.password(defaultpasswordEncoder().encode("admin"))
				.roles("USER", "ADMIN").build());
		return manager;
	}

	@Bean()
	public PasswordEncoder defaultpasswordEncoder(){
		return new BCryptPasswordEncoder();
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringerToolApplication.class, args);
	}

}
