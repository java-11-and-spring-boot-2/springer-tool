package net.boddin.springertool.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class Abo {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private long auftragsNummer;

    @NotNull
    @Length(min = 1)
    private String druckErzeugnis;

    private LocalDateTime time;

    public Abo() {
        time = LocalDateTime.now();
    }


    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Abo(String druckErzeugnis, long auftragsNummer) {
        this.druckErzeugnis = druckErzeugnis;
        this.auftragsNummer = auftragsNummer;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDruckErzeugnis() {
        return druckErzeugnis;
    }

    public void setDruckErzeugnis(String druckErzeugnis) {
        this.druckErzeugnis = druckErzeugnis;
    }

    public long getAuftragsNummer() {
        return auftragsNummer;
    }

    public void setAuftragsNummer(long auftragsNummer) {
        this.auftragsNummer = auftragsNummer;
    }
}
