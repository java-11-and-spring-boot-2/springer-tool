package net.boddin.springertool.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class AboDto {

    @JsonProperty(value = "bez")
    @NotEmpty(message = "bezeichnung.missing")
    private String bezeichnung;

    private long auftragsnummer;

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public long getAuftragsnummer() {
        return auftragsnummer;
    }

    public void setAuftragsnummer(long auftragsnummer) {
        this.auftragsnummer = auftragsnummer;
    }
}
